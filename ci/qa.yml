###### Default Stage Names and tags for CI

.stage-qa:
  stage: qa

.stage-sanity:
  stage: sanity

.stage-cleanup:
  stage: cleanup

.qa-tags:
  tags:
    - docker

##########################

.except-smoke:
  except:
    variables:
      - $SMOKE_ONLY

.except-full:
  except:
    variables:
      - $FULL_ONLY

.only-with-gitlab-address:
  only:
    variables:
      - $GITLAB_ADDRESS

.only-trigger-schedule-pipeline:
  only:
    variables:
      - $CI_PIPELINE_SOURCE == 'trigger'
      - $CI_PIPELINE_SOURCE == 'schedule'
      # Multi-project pipeline triggers are type 'pipeline'
      - $CI_PIPELINE_SOURCE == 'pipeline'

.qa:
  image: "${CI_REGISTRY}/gitlab-org/gitlab-build-images:gitlab-qa-alpine-ruby-2.6"
  services:
    - docker:19.03.0-dind
  extends:
    - .stage-qa
    - .qa-tags
    - .rspec-report-opts
  artifacts:
    when: always
    expire_in: 10d
    paths:
      - ./gitlab-qa-run-*
    reports:
      junit: gitlab-qa-run-*/**/rspec-*.xml
  variables:
    QA_LOG_PATH: "/home/gitlab/qa/tmp/debug-$CI_JOB_NAME.log"
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    QA_ARTIFACTS_DIR: $CI_PROJECT_DIR
    GITLAB_USERNAME: gitlab-qa
    SIGNUP_DISABLED: 'true'
    QA_DEBUG: 1
    # The password contains $ so we need to wrap around it
    # to workaround https://gitlab.com/gitlab-org/gitlab/issues/17069
    GITLAB_FORKER_PASSWORD: $GITLAB_FORKER_PASSWORD_INPUT
    QA_CAN_TEST_GIT_PROTOCOL_V2: 'false'
    QA_CAN_TEST_ADMIN_FEATURES: 'false'
  retry:
    max: 2
    when: runner_system_failure
  before_script:
    - export GITLAB_PASSWORD="$GITLAB_QA_PASSWORD"
  script:
    - gem install gitlab-qa --no-document
    - 'echo "Running: gitlab-qa $SCENARIO $RELEASE -- $TEST_OPTS $RSPEC_REPORT_OPTS"'
    - gitlab-qa $SCENARIO $RELEASE -- $TEST_OPTS $RSPEC_REPORT_OPTS

######################

.qa-smoke:
  extends:
    - .qa
    - .stage-sanity
  variables:
    TEST_OPTS: '--tag smoke'

.qa-smoke-quarantine:
  extends:
    - .qa
    - .stage-sanity
  variables:
    TEST_OPTS: '--tag smoke --tag quarantine'
  allow_failure: true

######################

.qa-api:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/api'

.ee-qa-api:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/ee/api'

.qa-api-quarantine:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/api qa/specs/features/ee/api --tag quarantine'
  allow_failure: true

.qa-browser_ui-quarantine:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/browser_ui qa/specs/features/ee/browser_ui --tag quarantine --tag ~orchestrated'
  allow_failure: true

.qa-browser_ui-1_manage:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/browser_ui/1_manage'

.ee-qa-browser_ui-1_manage:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/ee/browser_ui/1_manage'

.qa-browser_ui-2_plan:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/browser_ui/2_plan'

.ee-qa-browser_ui-2_plan:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/ee/browser_ui/2_plan'

.qa-browser_ui-3_create:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/browser_ui/3_create'

.ee-qa-browser_ui-3_create:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/ee/browser_ui/3_create'

.qa-browser_ui-4_verify:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/browser_ui/4_verify'

.qa-browser_ui-5_package:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/browser_ui/5_package'

.ee-qa-browser_ui-5_package:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/ee/browser_ui/5_package --tag orchestrated'

.qa-browser_ui-6_release:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/browser_ui/6_release'

.ee-qa-browser_ui-6_release:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/ee/browser_ui/6_release'

.qa-browser_ui-7_configure:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/browser_ui/7_configure'

.qa-browser_ui-secure:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/ee/browser_ui/secure'

.qa-browser_non-devops:
  extends:
    - .qa
  variables:
    TEST_OPTS: 'qa/specs/features/browser_ui/non_devops'
